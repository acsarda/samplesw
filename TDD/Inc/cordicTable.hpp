/* Esta implementacion viene de
 * ST DT0085 Design tip
 * https://www.st.com/resource/en/design_tip/dm00441302-coordinate-rotation-digital-computer-algorithm-cordic-to-compute-trigonometric-and-hyperbolic-functions-stmicroelectronics.pdf
 * */

#ifndef INC_CORDICTABLE_H_
#define INC_CORDICTABLE_H_

//CORDIC, 16 bits, 14 iterations
// 1.0 = 8192.000000 multiplication factor
// A = 1.743165 convergence angle (limit is 1.7432866 = 99.9deg)
// F = 1.646760 gain (limit is 1.64676025812107)
// 1/F = 0.607253 inverse gain (limit is 0.607252935008881)
// pi = 3.141593 (3.1415926536897932384626)

#define CORDIC_A 1.743165 // CORDIC convergence angle A
#define CORDIC_F 0x000034B2 // CORDIC gain F
#define CORDIC_1F 0x0000136F // CORDIC inverse gain 1/F
#define CORDIC_HALFPI 0x00003244
#define CORDIC_PI 0x00006488
#define CORDIC_TWOPI 0x0000C910
#define CORDIC_MUL 8192.000000 // CORDIC multiplication factor M = 2^13
#define CORDIC_MAXITER 14
 // lo siguiente lo agregue yo (Augusto), no lo genera el script
#define CORDIC_MUL_FACTOR 13
#define CORDIC_PI_1_3 CORDIC_PI/3       // pi / 3
#define CORDIC_PI_2_3 (CORDIC_PI*2)/3   // pi * 2 / 3
// #define CORDIC_PI_4_3 (CORDIC_PI*4)/3   // pi * 4 / 3
// #define CORDIC_PI_5_3 (CORDIC_PI*5)/3   // pi * 5 / 3

static int CORDIC_ZTBL[] = {
 0x00001922, 0x00000ED6, 0x000007D7, 0x000003FB, 0x000001FF, 0x00000100, 0x00000080, 0x00000040,
 0x00000020, 0x00000010, 0x00000008, 0x00000004, 0x00000002, 0x00000001 };
#endif /* INC_CORDICTABLE_H_ */

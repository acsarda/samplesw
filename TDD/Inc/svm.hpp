// Space vector modulation

#ifndef INC_SVM_H_
#define INC_SVM_H_

#include <cstdint>

/**
* a2q: a phase to q axis alignment
* a2d: a phase to d axis alignment
*/
enum {a2q = 0, a2d};

class SpaceVectorPWM
{
    public:
    SpaceVectorPWM(uint32_t busDCValue, uint8_t alignment, uint32_t Ts);
    
    void setVqVd(int32_t vq, int32_t vd, int32_t angle);

    // private:
    
    // // Atributos
    uint8_t alignment;  // alineacion de la tension con d o con q
    uint8_t arr;        // auto reload register
    uint32_t Vdc;       // valor del bus dc (i.e. Vcc, Vdc) 
    uint32_t VxMod;	    // = (2/3) * Vdc.
	uint32_t vRefMax;   // ten/sion maxima de referencia en el hexagono.
    uint32_t Ts;        // Periodo del PWM en cuentas

    int32_t vq, vd;         // tension en componentes q y d (Park)
    int32_t vAlpha, vBeta;  // tension en componentes alpha y beta (Clarke)
    int32_t angleAlphaBeta; // el angulo que forman las componentes alpha y beta
    int32_t vRef;  // tension de referencia en el hexagono
    uint8_t sector; // el sector del hexagono de svm
    uint32_t alpha; // el angulo alpha
    
    uint32_t t0, t1, t2;

    void cartesian2polar();
    void angle2sector();
    void alphaAngle();

    /**
    * @brief Transformacion de Parke a Clarke 
    * Referencia:
    * https://www.mathworks.com/help/physmod/sps/ref/parktoclarkeangletransform.html
    * @param alignment 
    */
    void park2clarke(int32_t angle);

    /**
    * @brief SVM - Calculo de los tiempos t0, t1 y t2 
    */
    void timesCalculation();    
};

#endif /* INC_SVM_H_ */ 

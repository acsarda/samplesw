#include "../Inc/svm.hpp"
#include "../Inc/cordicTable.hpp"
#include "../Inc/cordicTrigonometric.hpp"
#include <stdint.h>
#include <stdio.h>

SpaceVectorPWM::SpaceVectorPWM(uint32_t busDCValue, uint8_t alignment, uint32_t Ts)
{
    int32_t sin60, cos;

    this->alignment = alignment;
    this->Ts = Ts; 
    this->Vdc = busDCValue;
    this->VxMod = (this->Vdc << 1) / 3;

    CORDICsincos(CORDIC_PI_1_3, CORDIC_1F, &sin60, &cos);
    this->vRefMax = (VxMod * sin60) >> CORDIC_MUL_FACTOR;
}

void SpaceVectorPWM::setVqVd(int32_t vq, int32_t vd, int32_t angle)
{
    this->vq = vq;
    this->vd = vd;

    //  el orden en que las siguientes funciones son llamadas es importante
    park2clarke(angle);
    cartesian2polar();
    angle2sector();
    alphaAngle();
    timesCalculation();
}

void SpaceVectorPWM::park2clarke(int32_t angle)
{    
    int32_t sinAng, cosAng;
    CORDICsincos(angle, CORDIC_1F, &sinAng, &cosAng);

    if(alignment == a2q)
    {
        this->vAlpha =  ((sinAng * this->vd) >> CORDIC_MUL_FACTOR) + ((cosAng * this->vq) >> CORDIC_MUL_FACTOR);
        this->vBeta  = -((cosAng * this->vd) >> CORDIC_MUL_FACTOR) + ((sinAng * this->vq) >> CORDIC_MUL_FACTOR);   
    }
    else if(alignment == a2d)
    {
        this->vAlpha = ((cosAng * this->vd) >> CORDIC_MUL_FACTOR) - ((sinAng * this->vq) >> CORDIC_MUL_FACTOR);
        this->vBeta  = ((sinAng * this->vd) >> CORDIC_MUL_FACTOR) + ((cosAng * this->vq) >> CORDIC_MUL_FACTOR);
    }
}

void SpaceVectorPWM::cartesian2polar()
{
    CORDICatan2sqrt(&(this->angleAlphaBeta), &(this->vRef), this->vBeta, this->vAlpha);
}

void SpaceVectorPWM::angle2sector()
{
    if(this->angleAlphaBeta >= 0 && this->angleAlphaBeta < CORDIC_PI_1_3)
    {
        this->sector = 1;
    }
    else if(this->angleAlphaBeta >= CORDIC_PI_1_3 && this->angleAlphaBeta < CORDIC_PI_2_3)
    {
        this->sector = 2;
    }
    else if(this->angleAlphaBeta >= CORDIC_PI_2_3 && this->angleAlphaBeta < CORDIC_PI)
    {
        this->sector = 3;
    }
    else if(this->angleAlphaBeta >= - CORDIC_PI && this->angleAlphaBeta < - CORDIC_PI_2_3)
    {
        this->sector = 4;
    }
    else if(this->angleAlphaBeta >= - CORDIC_PI_2_3 && this->angleAlphaBeta < - CORDIC_PI_1_3)
    {
        this->sector = 5;
    }
    else if(this->angleAlphaBeta >= - CORDIC_PI_1_3 && this->angleAlphaBeta < 0)
    {
        this->sector = 6;
    }
}
void SpaceVectorPWM::alphaAngle()
{
    int8_t sectorAux;

    if(this->angleAlphaBeta >= 0)
    {
        this->alpha = this->angleAlphaBeta - (this->sector - 1) * CORDIC_PI_1_3;
    }
    else
    {
        switch (this->sector) 
        {
            case 4:
                sectorAux = -3;
                break;
            case 5:
                sectorAux = -2;
                break;
            case 6:
                sectorAux = -1;
                break;
        }
        this->alpha = this->angleAlphaBeta - sectorAux * CORDIC_PI_1_3;
    }
}    
void SpaceVectorPWM::timesCalculation()
{
    int32_t sin, cos;
	int32_t sin60;
	int32_t t;
	int32_t factor;

	if(vRef > vRefMax)
	{
		vRef = vRefMax;
	}

	CORDICsincos(CORDIC_PI_1_3, CORDIC_1F, &sin60, &cos);

	factor = vRef * Ts;
	factor /= VxMod;

	factor *= CORDIC_MUL;
	factor /= sin60;

	CORDICsincos(CORDIC_PI_1_3 - alpha, CORDIC_1F, &sin, &cos);
	t = sin * factor;
	t /= CORDIC_MUL;
	t1 = (t > Ts)? 0 : t;

	CORDICsincos(alpha, CORDIC_1F, &sin, &cos);
	t = sin * factor;
	t /= CORDIC_MUL;
	t2 = (t > Ts)? 0 : t;

	t0 = (Ts < t1 + t2)? 0 : Ts - (t1 + t2);
}


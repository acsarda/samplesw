import numpy as np
import matplotlib.pyplot as plt

m = np.sqrt(3) / 3
alpha = np.linspace(0, np.pi / 3, 1000)
Ts = 1800

t1 = np.sqrt(3) * m * np.sin((np.pi / 3) - alpha) * Ts / 2
t2 = np.sqrt(3) * m * np.sin(alpha) * Ts / 2

plt.plot(np.arange(t1.size), t1)
plt.plot(np.arange(t2.size), t2)
plt.show()

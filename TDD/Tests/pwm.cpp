#include "CppUTest/TestHarness.h"

#include <CppUTest/UtestMacros.h>
#include <cstdint>
#include <cmath>
#include "stdint.h"
// #include "iostream"

#include "../Inc/cordicTable.hpp"
#include "../Inc/cordicTrigonometric.hpp"
#include "../Inc/svm.hpp"

TEST_GROUP(PWM)
{
    double pi = 3.141592653589793;
    enum {a2q = 0, a2d};

    uint8_t autoReloadRegister = 11;
    uint32_t Vdc = 12 * CORDIC_MUL;
    uint32_t Ts = 1 << 11;
    SpaceVectorPWM sv{Vdc, a2d, Ts};

    uint8_t angleDbl2sector(double angle)
    {
        uint8_t sector = 0;

        if(angle >= 0 && angle < (pi / 3))
        {
            sector = 1;
        }
        else if(angle >= (pi / 3) && angle < (pi * 2 / 3))
        {
            sector = 2;
        }
        else if(angle >= (pi * 2 / 3) && angle <= pi)
        {
            sector = 3;
        }
        else if(angle >= - pi && angle < -(pi * 2 / 3))
        {
            sector = 4;
        }
        else if(angle >= -(pi * 2 / 3) && angle < -(pi * 1 / 3))
        {
            sector = 5;
        }
        else if(angle >= -(pi * 1 / 3) && angle < 0)
        {
            sector = 6;
        }
        return sector;
    }
    
    std::array<std::array<double, 2>, 2> sector2tensionVectorsDbl(uint8_t sector, double Vdc)
    {
        double v1real, v1imag;
        double v2real, v2imag;

        double angle2 = (double)sector * pi / 3.0;
        double angle1 = angle2 - pi / 3.0;
        
        v1real = std::cos(angle1) * (2.0 / 3.0) * Vdc;
        v1imag = std::sin(angle1) * (2.0 / 3.0) * Vdc;

        v2real = std::cos(angle2) * (2.0 / 3.0) * Vdc;
        v2imag = std::sin(angle2) * (2.0 / 3.0) * Vdc;

        auto v1 = std::array<double, 2> {v1real, v1imag};
        auto v2 = std::array<double, 2> {v2real, v2imag};

        return std::array<std::array<double, 2>, 2> {v1, v2};
    }

    std::array<std::array<int32_t, 2>, 2> sector2tensionVectors(uint8_t sector, int32_t Vdc)
    {
        int32_t v1real, v1imag;
        int32_t v2real, v2imag;
        
        int32_t angle2 = (sector * CORDIC_PI ) / 3;
        int32_t angle1 = angle2 - CORDIC_PI / 3;

        int32_t sinCnt, cosCnt;

        CORDICsincos(angle1, CORDIC_1F, &sinCnt, &cosCnt);
        v1real = (cosCnt * Vdc) / CORDIC_MUL;
        v1real *= 2;
        v1real /= 3;

        v1imag = (sinCnt * Vdc) / CORDIC_MUL;
        v1imag *= 2;
        v1imag /= 3;

        CORDICsincos(angle2, CORDIC_1F, &sinCnt, &cosCnt);
        v2real = (cosCnt * Vdc) / CORDIC_MUL;
        v2real *= 2;
        v2real /= 3;

        v2imag = (sinCnt * Vdc) / CORDIC_MUL;
        v2imag *= 2;
        v2imag /= 3;

        auto v1 = std::array<int32_t, 2> {v1real, v1imag};
        auto v2 = std::array<int32_t, 2> {v2real, v2imag};

        return std::array<std::array<int32_t, 2>, 2> {v1, v2};
    }

    double alphaAngleDbl(uint8_t sector, double angle)
    {
        int8_t sectorAux;
        double alpha;
        if(angle >= 0)
        {
            alpha = angle - (sector - 1) * pi / 3;            
        }
        else
        {
            switch(sector)
            {
                case 4:
                    sectorAux = -3;
                    break;
                case 5:
                    sectorAux = -2;
                    break;
                case 6:
                    sectorAux = -1;
                    break;
            }
            alpha = angle - sectorAux * pi / 3;
        }
        
        return alpha;
    }

    void park2clarkeDbl(uint8_t alignment, double angle, double d, double q, double *alpha, double *beta)
    {
        // Referencia:
        // https://www.mathworks.com/help/physmod/sps/ref/parktoclarkeangletransform.html
        
        if(alignment == a2q)
        {
            *alpha = std::sin(angle) * d + std::cos(angle) * q;
            *beta = -std::cos(angle) * d + std::sin(angle) * q;   
        }
        else if(alignment == a2d)
        {
            *alpha = std::cos(angle) * d - std::sin(angle) * q;
            *beta  = std::sin(angle) * d + std::cos(angle) * q;
        }
    }

    std::array<double, 3> timesCalculationDbl(double Ts, double Vdc, double Vref, double alpha)
    {
        double factor = Vref * Ts / (Vdc * (2. / 3.) * std::sin(pi / 3.));

        double t1 = factor * std::sin((pi / 3.) - alpha);
        double t2 = factor * std::sin(alpha);
        double t0 = Ts - (t1 + t2);
        return std::array<double, 3> {t0, t1, t2};
    }
    
};

TEST(PWM, coord2polarTransform)
{
    double x, y;
    double mod, ang;

    int32_t xCnt, yCnt;
    int32_t angCnt;
    uint32_t modCnt;

    uint32_t points = 1000;

    double threshold = .01;

    srand(time(NULL)); // use current time as seed for random generator
    
    for(uint32_t idx = 0; idx < points; idx++)
    {
        x = ((double)rand() / RAND_MAX) * 12 - 24;
        y = ((double)rand() / RAND_MAX) * 12 - 24;
        mod = std::sqrt(std::pow(x, 2) + std::pow(y, 2));
        ang = std::atan2(y, x);

        xCnt = (int32_t)(x * CORDIC_MUL);
        yCnt = (int32_t)(y * CORDIC_MUL);
        // cartesian2polar(xCnt, yCnt, &modCnt, &angCnt);
        sv.vAlpha = xCnt;
        sv.vBeta = yCnt;
        sv.cartesian2polar();

        DOUBLES_EQUAL(mod, ((double)sv.vRef) / CORDIC_F, threshold);
        DOUBLES_EQUAL(ang, ((double)sv.angleAlphaBeta) / CORDIC_MUL, threshold);
    }
}

TEST(PWM, sectorDetermination)
{
    int32_t angleCnt;
    uint32_t points = 10000;
    double angle;

    for(uint32_t idx = 0; idx < points; idx++)
    {
        
        angle = idx * 2 * pi / points - pi;
        angleCnt = (int32_t)(angle * CORDIC_MUL);
        sv.angleAlphaBeta = angleCnt;
        sv.angle2sector();
        CHECK_EQUAL(angleDbl2sector(angle), sv.sector);
    }
}

TEST(PWM, angleAlpha)
{
    uint32_t angleCnt;
    uint32_t alphaCnt;
    uint32_t points = 1000;
    
    double angle;
    double alpha;

    for(uint32_t idx = 0; idx < points; idx++)
    {
        angle = idx * 2 * pi / points - pi;
        angleCnt = angle * CORDIC_MUL;

        alpha = alphaAngleDbl(angleDbl2sector(angle), angle);

        sv.angleAlphaBeta = angleCnt;
        sv.angle2sector();
        sv.alphaAngle();
                
        DOUBLES_EQUAL(alpha, (double)sv.alpha / CORDIC_MUL, 0.001);
        CHECK_TRUE(alpha >= 0.0);
        CHECK_TRUE(alpha <= pi / 3);
    }
}

TEST(PWM, sinCosFunctions)
{
    // en realidad seria un test especifico de cordic
    int32_t angleCnt;
    int32_t sinCnt, cosCnt;
    uint32_t points = 1000;
    
    double angle;
    double threshold = .001;

    for(uint32_t idx = 0; idx < points; idx++)
    {
        angle = idx * 2 * pi / points - pi;
        angleCnt = (int32_t)(angle * CORDIC_MUL);

        CORDICsincos(angleCnt, CORDIC_1F, &sinCnt, &cosCnt);

        DOUBLES_EQUAL(std::sin(angle), ((double)(sinCnt)) / CORDIC_MUL, threshold);
        DOUBLES_EQUAL(std::cos(angle), ((double)(cosCnt)) / CORDIC_MUL, threshold);
    }
}

TEST(PWM, park2clarkeTransforms)
{
    double d, q, angle, alpha, beta;

    int32_t dCnt, qCnt, angleCnt;

    double threshold = .1;

    srand(time(NULL)); // use current time as seed for random generator
    
    angle = ((double)rand() / RAND_MAX) * 2 * pi - pi;
    d = std::cos(angle);
    q = std::sin(angle);

    dCnt = (int32_t)(d * CORDIC_MUL);
    qCnt = (int32_t)(q * CORDIC_MUL);
    angleCnt = (int32_t)(angle * CORDIC_MUL);

    sv.vd = dCnt;
    sv.vq = qCnt;
    
    sv.park2clarke(angleCnt);

    park2clarkeDbl(a2d, angle, d, q, &alpha, &beta);

    DOUBLES_EQUAL(alpha, (double)sv.vAlpha / CORDIC_MUL, threshold);
    DOUBLES_EQUAL(beta, (double)sv.vBeta / CORDIC_MUL, threshold);
}

TEST(PWM, timesCalculationOverTime)
{
    double Fe = 5000.0;
    double Te = 1 / Fe;

    double F_PWM = 17578.0;
    double TsPwm = 1 / F_PWM;
    
    double Vdc = 12.0;
    double vRefMax = Vdc * (2. / 3.) * (std::sqrt(3) / 2.);
    
    double omega = 6. * 900. * 2. * pi / 60.;
    double theta;

    double tf = 1; // 1s
    double t = 0;

    double vd, vq, vAlpha, vBeta;

    // ----------------------------------------------------

    uint32_t VdcCnt = Vdc * CORDIC_F;
    uint32_t TsPwmCnt = 1 << 11;
    int32_t vdCnt, vqCnt, thetaCnt;
    SpaceVectorPWM sv(VdcCnt, a2d, TsPwmCnt);

    // ----------------------------------------------------

    vd = 0.0;
    vq = 2.;
   
    vqCnt = vq * CORDIC_MUL;
    vdCnt = vd * CORDIC_MUL;

    while(t < tf)
    {
        theta += omega * Te; // angulo electrico
        if(theta > pi)
        {
            theta -= 2 * pi;
        }

        park2clarkeDbl(a2d, theta, vd, vq, &vAlpha, &vBeta);
        auto vRef = std::sqrt(std::pow(vAlpha, 2) + std::pow(vBeta, 2));
        auto angleRef = std::atan2(vBeta, vAlpha);

        auto sector = angleDbl2sector(angleRef);
        auto alpha = alphaAngleDbl(sector, angleRef);
        auto t0t1t2 = timesCalculationDbl(TsPwm, Vdc, vRef, alpha);
        auto t0 = t0t1t2.at(0);
        auto t1 = t0t1t2.at(1);
        auto t2 = t0t1t2.at(2);

        auto v1v2 = sector2tensionVectorsDbl(sector, Vdc);

        auto v1 = v1v2.at(0);
        auto v2 = v1v2.at(1);

        DOUBLES_EQUAL(TsPwm, t0 + t1 + t2, 1e-9);

        auto real = (v1.at(0) * t1 + v2.at(0) * t2) / TsPwm;
        auto imag = (v1.at(1) * t1 + v2.at(1) * t2) / TsPwm;
        DOUBLES_EQUAL(vAlpha, real, 1e-6);
        DOUBLES_EQUAL(vBeta,  imag, 1e-6);
        // ----------------------------------------------------------------

        thetaCnt = theta * CORDIC_MUL;
        sv.setVqVd(vqCnt, vdCnt, thetaCnt);
        double t1p = (double) sv.t1 / (double) sv.Ts;
        double t2p = (double) sv.t2 / (double) sv.Ts;
        double t0p = (double) sv.t0 / (double) sv.Ts;

        printf("vAlpha = %lf | vAlpha uc = %lf\n", vAlpha, (double)sv.vAlpha / CORDIC_MUL);
        printf("vBeta  = %lf | vBeta  uc = %lf\n", vBeta , (double)sv.vBeta  / CORDIC_MUL);
        printf("vRef = %lf   | vRef uc = %lf\n", vRef, (double)sv.vRef / CORDIC_F);
        
        auto real_uc = v1.at(0) * t1p + v2.at(0) * t2p;
        auto imag_uc = v1.at(1) * t1p + v2.at(1) * t2p;
        printf("%lf, %lf\n",   vAlpha, real_uc);
        printf("%lf, %lf\n\n", vBeta,  imag_uc);
        //DOUBLES_EQUAL(vAlpha, real_uc, 1e-1);
        //DOUBLES_EQUAL(vBeta,  imag_uc, 1e-1);

        printf("time = %lf s, angle = %lf [deg]\n\n", t, theta * 180. / pi);
        //printf("%lf, %lf\n",   vAlpha, real);
        //printf("%lf, %lf\n\n", vBeta, imag);

        t += Te;
    }
}

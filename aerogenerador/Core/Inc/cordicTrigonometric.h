#ifndef INC_CORDICTRIGONOMETRIC_H_
#define INC_CORDICTRIGONOMETRIC_H_

#include "cordicTable.h"
#include "main.h"

void CORDICsincos(int32_t a, int32_t m, int32_t *s, int32_t *c);
void CORDICatan2sqrt(int32_t *a, int32_t *m, int32_t y, int32_t x);
void CORDICatansqrt(int32_t *a, int32_t *m, int32_t y, int32_t x);

#endif /* INC_CORDICTRIGONOMETRIC_H_ */

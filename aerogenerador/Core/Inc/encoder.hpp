/*
 * encoder.hpp
 *
 *  Created on: Jul 17, 2022
 *      Author: augusto
 */

#ifndef INC_ENCODER_HPP_
#define INC_ENCODER_HPP_

#ifdef __cplusplus
extern "C" {
#endif

#include "main.h"
#include "cordicTable.h"

#ifdef __cplusplus
}
#endif

#define POLES_PAIRS 6

class Encoder
{
	public:
		Encoder(TIM_HandleTypeDef * htim);

		/*
		 * @brief
		 * Returns the mechanical position in radians multiply by CORDIC_MUL
		 * */
		int32_t getMechanicalPosition()
		{

			return mechanicalPosition;
		}

		/*
		 * @brief
		 * Returns the electrical position in radians multiply by CORDIC_MUL*/
		int32_t getElectricalPosition()
		{
			return electricalPosition;
		}

		/*
		 * @brief
		 * Returns the mechanical speed in rad / s multiply by CORDIC_MUL
		 * */
		uint32_t getMechanicalSpeed()
		{
			return mechanicalSpeed;
		}

		/*
		 * @brief
		 * Returns the electrical speed (frequency) in rad / s multiply by CORDIC_MUL
		 * */
		uint32_t getElectricalSpeed()
		{
			return electricalSpeed;

		}

		void zSignalEvent();
		void signalDataProcessing();
		void calculateSpeed();

	private:
		TIM_HandleTypeDef * timHandler;
		int32_t mechanicalPosition = RESET;
		int32_t electricalPosition = RESET;
		uint32_t mechanicalSpeed = RESET;
		uint32_t electricalSpeed = RESET;
		uint32_t cntPrevious, cntActual;
		volatile int32_t electricPositionOffset = 9046;
		int32_t mechanicalPositionOffset = 0;

		void resetCounter();



		void calculateMechanicalPosition(int32_t counter);
		void calculateElectricalPosition(int32_t counter);
};

#endif /* INC_ENCODER_HPP_ */

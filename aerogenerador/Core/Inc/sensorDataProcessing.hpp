/*
 * sensorDataProcessing.hpp
 *
 *  Created on: Nov 26, 2022
 *      Author: augusto
 */
/*
#ifndef INC_SENSORDATAPROCESSING_HPP_
#define INC_SENSORDATAPROCESSING_HPP_

#include "currents.hpp"
#include "encoder.hpp"

#include "main.h"


class SensorDataProcessing
{
	public:
		SensorDataProcessing();
		void encoderInit();
		void currentInit();

	private:
		Current currents;
		Encoder encoder;
};
#endif /* INC_SENSORDATAPROCESSING_HPP_ */

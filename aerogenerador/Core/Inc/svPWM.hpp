/*
 * svPWM.hpp
 *
 *  Created on: Jun 20, 2022
 *      Author: augusto
 */

#ifndef INC_SVPWM_HPP_
#define INC_SVPWM_HPP_

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"

#ifdef __cplusplus
}
#endif


/**
* a2q: a phase to q axis alignment
* a2d: a phase to d axis alignment
*/
enum {a2q = 0, a2d}; // axis alignments

class SpaceVectorPWM
{
    public:

		/**
		 * Class constructor
		 * @param autoReloadRegister: the ARR of the timer used for the PWM
		 * @param busDCValue: tension del bus de DC. Me parece que esta mal porque lo pense como motor /FIXME
		 * @param alignment: 0 or 1, for phase a align to q axis or to d axis, respectively.
		 */
		SpaceVectorPWM(TIM_HandleTypeDef * htim, uint8_t channelA, uint8_t channelB, uint8_t channelC, uint32_t busDCValue, uint8_t alignment, uint32_t Ts);

		/**
		 * Set the vq and vd reference tensions.
		 * When the micro is working in a control loop, these will be set by the control algorithm (or an interface with it) TODO
		 * @param vq: q tension
		 * @param vd: d tension
		 * #param electricalAngle: electrical angle position to perform the necessary transformation
		 */
		void startPWMOutput();
		void update(int32_t vq, int32_t vd, int32_t electricalAngle);
		void updatePWMOutput();

		int32_t getvAlpha()
		{
			return vAlpha;
		}

		int32_t getvBeta()
		{
			return vBeta;
		}

		int32_t getAngleAlphaBeta()
		{
			return angleAlphaBeta;
		}

		int32_t getvRef()
		{
			return vRef;
		}

		uint8_t getSector()
		{
			return sector;
		}

		uint32_t getAplha()
		{
			return alpha;
		}

		uint32_t gett0()
		{
			return t0;
		}

		uint32_t gett1()
		{
			return t1;
		}

		uint32_t gett2()
		{
			return t2;
		}

		uint32_t * getPulse()
		{
			return pulse;
		}


    private:
		TIM_HandleTypeDef * htim; // tim handler
		uint8_t channels[3];

		// attributes for the SVM technique
		uint8_t alignment;  	// alineacion de la tension con d o con q
		uint8_t arr;        	// pwm auto reload register
		int32_t Vdc;       	// valor del bus dc (i.e. Vcc, Vdc)
		int32_t VxMod;			// = (2/3) * Vdc.
		int32_t vRefMax;       // ten/sion maxima de referencia en el hexagono.
		uint32_t Ts;        	// Periodo del PWM en cuentas
		uint32_t Ts2; 			// Ts / 2

		int32_t vq, vd;         // tension en componentes q y d (Park)
		int32_t vAlpha, vBeta;  // tension en componentes alpha y beta (Clarke)
		int32_t angleAlphaBeta; // el angulo ue forman las componentes alpha y beta
		int32_t vRef;           // tension de referencia en el hexagono
		uint8_t sector;         // el sector del hexagono de svm
		uint32_t alpha;         // el angulo alpha

		uint32_t t0, t1, t2;
		uint32_t pulse[3];

		// methods for the SVM technique
		void cartesian2polar();
		void angle2sector();
		void alphaAngle();

		/**
		* @brief Transformacion de Parke a Clarke
		* Referencia:
		* https://www.mathworks.com/help/physmod/sps/ref/parktoclarkeangletransform.html
		* @param alignment
		*/
		void park2clarke(int32_t electricalAngle);

		/**
		* @brief SVM - Calculo de los tiempos t0, t1 y t2
		*/
		void timesCalculation();

		void modulation();

		void setPulse(uint8_t channel, uint32_t pulse);

		// methods for the measurements


};
#endif /* INC_SVPWM_HPP_ */

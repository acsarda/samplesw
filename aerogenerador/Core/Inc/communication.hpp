/*
 * communication2.hpp
 *
 *  Created on: Nov 14, 2022
 *      Author: augusto
 */

#ifndef INC_COMMUNICATION_HPP_
#define INC_COMMUNICATION_HPP_

#include "stm32f1xx_hal.h"
#include "encoder.hpp"
#include "currents.hpp"
#include "controller.hpp"

#define LENGTHMENU 3
#define LENGTHCONTINUOUS (7 * 2 + 4 + 2) // 7 variables 16 bits + 1 var de 32 bits + header + checksum
#define LENGTHBATCH (10 * 2 + 3)   // 10 datos de una variables + header + id + checksum
#define LENGTHWORD2 2
#define LENGTHWORD4 4
#define HEADER 0xAA

enum states {StateMENU = 0, StateCONTINUOUS, StateBATCH};
enum batchVariables {ia_ib_elecAngle = 10, iqRef_iq_id, mechSpeedRef_mechSpeed};
enum batchOrder {variable1st = 1, variable2nd, variable3rd};

class Communication2
{
	public:
		//Communication2(UART_HandleTypeDef *h, Encoder *enconderInstance, Current *currentInstance, DirectController *idControllerInstance, QuadratureController *iqControllerInstance);
		Communication2(UART_HandleTypeDef *h, Encoder *encoderInstance, Current *currentInstance, Controller *velControllerInstance, DirectController *idControllerInstance, QuadratureController *iqControllerInstance, int64_t *power, int32_t *mechanicalSpeedRef);
		UART_HandleTypeDef *huart;

		struct {bool receiveFree;
		        bool transmitionFree;
		        bool loadingBuffersBatch;
				} flags;

		uint8_t bufferBatch2transmit;

		void checkState();
		void response(bool timeFlag);

		uint8_t getState()
		{
			return state;
		}

		Encoder *encoderInstance;
		Current *currentInstance;
		Controller *velControllerInstance;
		DirectController *idControllerInstance;
		QuadratureController *iqControllerInstance;
		int64_t *power;
		int32_t *mechanicalSpeedRef;

		uint8_t state;
		uint8_t variables;
		uint32_t setPointAngularSpeed;
		uint8_t bufferMenu[LENGTHMENU];
		uint8_t bufferContinuous[LENGTHCONTINUOUS];
		uint8_t bufferVariable1[LENGTHBATCH];
		uint8_t bufferVariable2[LENGTHBATCH];
		uint8_t bufferVariable3[LENGTHBATCH];

		uint8_t positionBufferBatch;


		void word2buffer(int16_t word, uint8_t *buffer);
		void word2buffer(uint16_t word, uint8_t *buffer);
		void word2buffer(uint32_t word, uint8_t *buffer);

		void setContinuousData(
							   const uint32_t ,
							   const int16_t ,
							   const int16_t,
							   const int16_t ,
							   const int16_t ,
							   const int16_t ,
							   const int16_t,
							   const int16_t );
		void loadBuffersBatch();
		void responseContinuousMechanicalSpeedRef();
		void responseContinuousMPPT();
		void responseBatch();
};
#endif /* INC_COMMUNICATION_HPP_ */

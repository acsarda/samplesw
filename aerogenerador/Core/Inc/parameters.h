/*
 * parameters.h
 *
 *  Created on: Jun 27, 2022
 *      Author: augusto
 */

#ifndef INC_PARAMETERS_H_
#define INC_PARAMETERS_H_

#include <cordicTable.h>

#define PWM_AUTORELOADREGISTER 11
#define VDC 12*CORDIC_MUL

// Actuadores
#define NUM_PWM 3

#endif /* INC_PARAMETERS_H_ */

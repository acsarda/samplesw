/*
 * actuator.h
 *
 *  Created on: Jun 27, 2022
 *      Author: augusto
 */

#ifndef INC_ACTUATORS_HPP_
#define INC_ACTUATORS_HPP_

#include "stdint.h"
#include "stm32f1xx_hal.h"

namespace actuator
{

class PWM
{
	public:
		void init(TIM_HandleTypeDef * handlerTIM, uint8_t channel);
		void setPulse(uint32_t pulseDC);
		void updatePWM();
	protected:
		TIM_HandleTypeDef * handlerTIM;
		uint8_t channel;
		uint32_t pulse = 0;
};
} // namespace actuator
#endif /* INC_ACTUATOR_H_ */

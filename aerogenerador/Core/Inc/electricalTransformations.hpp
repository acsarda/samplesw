/*
 * electricalTransformations.h
 *
 *  Created on: Jul 18, 2022
 *      Author: augusto
 */

#ifndef INC_ELECTRICALTRANSFORMATIONS_HPP_
#define INC_ELECTRICALTRANSFORMATIONS_HPP_

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"

#ifdef __cplusplus
}
#endif

namespace transformations
{

enum {a2q = 0, a2d}; // axis alignments

void park2clarke(int32_t vd, int32_t vq, int32_t electricalAngle,
				 uint8_t alignment,
				 int32_t * vAlpha, int32_t * vBeta);

void abc2dq(int32_t a, int32_t b, int32_t c, int32_t electricalAngle,
			uint8_t alignment,
			int32_t * d, int32_t * q);
} // namespace trasformations

#endif /* INC_ELECTRICALTRANSFORMATIONS_HPP_ */

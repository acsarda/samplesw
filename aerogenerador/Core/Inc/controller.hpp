/*
 * controller.hpp
 *
 *  Created on: Dec 11, 2022
 *      Author: augusto
 */

#ifndef INC_CONTROLLER_HPP_
#define INC_CONTROLLER_HPP_

#ifdef __cplusplus
extern "C" {
#endif

#include "main.h"
#include "cordicTable.h"

#ifdef __cplusplus
}
#endif

#define L 1243         // Lq = Ld = 1.5 * 0.79 mH ~ 2^20
#define PSI_PM 30409   // = 0.029 Wb ~ 2^20
// #define FACTOR 1048576 // = 2^20
#define FACTOR 20

class Controller
{
	public:
		Controller(int32_t kp, int32_t ki, int32_t lowerLimit, int32_t uppperLimit);

		void setReference(int32_t reference)
		{
			this->reference = reference;
		}

		int32_t getReference()
		{
			return reference;
		}

		int32_t getControlAction()
		{
			return controlAction;
		}
		void reset()
		{
			integraAction1 = 0;
			controlAction = 0;
		}
		

	protected:

		int32_t error;                  // t = k
	
	//	int32_t intError;               // t = k
		int64_t integraAction1;         // t = k - 1
		int32_t controlAction;          // i.e. u
		int32_t feedforward; // i.e. u + termino de desacople
		int32_t kp, ki, lowerLimit, upperLimit;

	public:
		int32_t reference;
		int32_t controlPI(int32_t current);
};

class DirectController : public Controller
{
	public:
		using Controller::Controller; // reuse constructor
		void calculateDecoupling(int32_t angularSpeed, int32_t iqCurrent);
};

class QuadratureController : public Controller
{
	public:

		using Controller::Controller; // reuse constructor
		void calculateDecoupling(int32_t angularSpeed, int32_t idCurrent);
};
#endif /* INC_CONTROLLER_HPP_ */

/*
 * currents.hpp
 *
 *  Created on: Jul 19, 2022
 *      Author: augusto
 */

#ifndef INC_CURRENTS_HPP_
#define INC_CURRENTS_HPP_

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"

#ifdef __cplusplus
}
#endif

#define ADC1_COUNT_MAX 3809
#define ADC2_COUNT_MAX 3791

#define ADC1_OFFSET_MEAN 1915//1906
#define ADC2_OFFSET_MEAN 1900

#define ADC1_OFFSET_STD 2
#define ADC2_OFFSET_STD 3
#define FILTER_A 5952 // corte 1/4 freq de muestreo
#define FILTER_B  1120


#define SENSOR_RANGE 60

class Current
{
	public:
		/**
		 * Class constructor
		 * @param alignment: 0 or 1, for phase a align to q axis or to d axis, respectively.
		 */
		Current(uint8_t alignment);

		/**
		 * Set the phases currents ia, ib and ic, as well as the Park id, iq currents.
		 * @param vaCnt: the data register of the adc that measures phase a current (the measurement is a voltage)
		 * @param vbCnt: the data register of the adc that measures phase b current (the measurement is a voltage)
		 * @param electricalAngle: the electrical angle position. This is calculated by the encoder or the estimator
		 */
		void setCurrents(uint32_t vaCnt, uint32_t vbCnt);

		int32_t filter(int32_t, int32_t a, int32_t b, int32_t* state);
		int32_t getIq()
		{
			return iq;
		}
		int32_t getId()
		{
			return id;
		}

		int32_t getIa()
		{
			return ia;
		}

		int32_t getIb()
		{
			return ib;
		}

		int32_t getIc()
		{
			return ic;
		}
		void calculateCurrents(int32_t electricalAngle);

	private:
		uint8_t alignment;
		int32_t ia, ib, ic;
		int32_t id, iq , vaCnt,vbCnt;
		int32_t filA[2],filB[2];


		/**
		 * Calibration of the adc measurement
		 * The adc measurement is a voltage in the data register of the adc (16 bit register)
		 * This voltage value is converted to a signed current value
		 * @param v: voltage value converter by the adc; i.e. the data register of the adc.
		 */
		int32_t calibration(int32_t adcCount, int32_t offsetMean, int32_t offsetStd);
};

#endif /* INC_CURRENTS_HPP_ */

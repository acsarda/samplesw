/*
 * controller.cpp
 *
 *  Created on: Dec 11, 2022
 *      Author: augusto
 */

#include "controller.hpp"

Controller::Controller(int32_t kp, int32_t ki, int32_t lowerLimit, int32_t uppperLimit)
{
	this-> lowerLimit = lowerLimit;
	this->upperLimit = uppperLimit;
	this->kp = kp; // ~ CORDIC_MUL
	this->ki = ki; // ~ CORDIC_MUL. Este ki ya esta multiplicado por el periodo de muestreo (i.e. = ki * Ts)
	this->integraAction1 = 0;
	feedforward = 0;
}

int32_t Controller::controlPI(int32_t current)
{
	/*
	 * controlAction = termProp + termInt
	 * termProp = kp * error
	 * termInt  = ki * intError
	 */
	int32_t out;
	int64_t  termInt,termProp;
	error = reference - current; // ~ CORDIC_MUL

	termProp = (kp * (int64_t)error) >> CORDIC_MUL_FACTOR; // ~ CORDIC_MUL

	termInt = (ki * error);    // ~ CORDIC_MUL^2 , integro usando Euler
	termInt += integraAction1; // ~ CORDIC_MUL^2

	out = termProp + (termInt >> CORDIC_MUL_FACTOR) + feedforward;
	
	if(out > upperLimit)
	{
		out = upperLimit;
		if(error < 0)
		{
			integraAction1 = termInt;
		}
	}
	else if(out < lowerLimit)
	{
		out = lowerLimit;
		if(error > 0)
		{
			integraAction1 = termInt;
		}
	}
	else
	{
		integraAction1 = termInt;
	}
	controlAction = out;
	return out;

}

void DirectController::calculateDecoupling(int32_t angularSpeed,  int32_t iqCurrent)
{
	int64_t tension; // el termino de desacople

	tension = iqCurrent * angularSpeed;	        // ~ CORDIC_MUL^2
	tension >>= CORDIC_MUL_FACTOR;				// ~ CORDIC_MUL
	tension *= L;                               // ~ CORDIC_MUL * FACTOR
	tension >>= FACTOR;                         // ~ CORDIC_MUL

	tension *= -1;

	feedforward = (int32_t)tension;
	return;
}
void QuadratureController::calculateDecoupling(int32_t angularSpeed, int32_t idCurrent)
{
	int64_t tension; // el termino de desacople

	tension = (L * idCurrent) + (PSI_PM * CORDIC_MUL); // ~ CORDIC_MUL * FACTOR
	tension >>= FACTOR;                                // ~ CORDIC_MUL
	tension *= angularSpeed;                           // ~ CORDIC_MUL^2
	tension >>= CORDIC_MUL_FACTOR;                     // ~ CORDIC_MUL

	feedforward = (int32_t)tension;
	return;
}

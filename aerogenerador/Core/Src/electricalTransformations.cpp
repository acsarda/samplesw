/*
 * electricalTransformations.c
 *
 *  Created on: Jul 18, 2022
 *      Author: augusto
 */

#include "electricalTransformations.hpp"

extern "C"
{
#include "cordicTrigonometric.h"
}

void transformations::park2clarke(int32_t vd, int32_t vq, int32_t electricalAngle,
								  uint8_t alignment,
								  int32_t * vAlpha, int32_t * vBeta)
{
	int32_t sinAng, cosAng;
	CORDICsincos(electricalAngle, CORDIC_1F, &sinAng, &cosAng);

	if(alignment == a2q)
	{
		*vAlpha =  ((sinAng * vd) >> CORDIC_MUL_FACTOR) + ((cosAng * vq) >> CORDIC_MUL_FACTOR);
		*vBeta  = -((cosAng * vd) >> CORDIC_MUL_FACTOR) + ((sinAng * vq) >> CORDIC_MUL_FACTOR);
	}
	else if(alignment == a2d)
	{
		*vAlpha = ((cosAng * vd) >> CORDIC_MUL_FACTOR) - ((sinAng * vq) >> CORDIC_MUL_FACTOR);
		*vBeta  = ((sinAng * vd) >> CORDIC_MUL_FACTOR) + ((cosAng * vq) >> CORDIC_MUL_FACTOR);
	}
}

void transformations::abc2dq(int32_t a, int32_t b, int32_t c, int32_t electricalAngle,
							 uint8_t alignment,
							 int32_t * d, int32_t * q)
{
	/*
	 * Reference
	 * https://www.mathworks.com/help/sps/ref/parktransform.html
	 * */
	int32_t sinAng, cosAng;
	int32_t sinAng_minus_2_3_pi, cosAng_minus_2_3_pi;
	int32_t sinAng_plus_2_3_pi, cosAng_plus_2_3_pi;

	CORDICsincos(electricalAngle, CORDIC_1F,                 &sinAng,              &cosAng);
	CORDICsincos(electricalAngle - CORDIC_PI_2_3, CORDIC_1F, &sinAng_minus_2_3_pi, &cosAng_minus_2_3_pi);
	CORDICsincos(electricalAngle + CORDIC_PI_2_3, CORDIC_1F, &sinAng_plus_2_3_pi,  &cosAng_plus_2_3_pi);

	if(alignment == a2q)
	{
		*d =  ((sinAng * a) >> CORDIC_MUL_FACTOR) + ((sinAng_minus_2_3_pi * b) >> CORDIC_MUL_FACTOR) + ((sinAng_plus_2_3_pi * c) >> CORDIC_MUL_FACTOR);
		*q =  ((cosAng * a) >> CORDIC_MUL_FACTOR) + ((cosAng_minus_2_3_pi * b) >> CORDIC_MUL_FACTOR) + ((cosAng_plus_2_3_pi * c) >> CORDIC_MUL_FACTOR);
	}
	else if(alignment == a2d)
	{
		*d =  ((cosAng * a) >> CORDIC_MUL_FACTOR) + ((cosAng_minus_2_3_pi * b) >> CORDIC_MUL_FACTOR) + ((cosAng_plus_2_3_pi * c) >> CORDIC_MUL_FACTOR);
		*q =  ((sinAng * a) >> CORDIC_MUL_FACTOR) + ((sinAng_minus_2_3_pi * b) >> CORDIC_MUL_FACTOR) + ((sinAng_plus_2_3_pi * c) >> CORDIC_MUL_FACTOR);
		*q *= (-1);
	}

	*q *= 2;
	*q /= 3;

	*d *= 2;
	*d /= 3;
}

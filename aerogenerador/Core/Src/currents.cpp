/*
 * currents.cpp
 *
 *  Created on: Jul 19, 2022
 *      Author: augusto
 */
#include "currents.hpp"
#include "electricalTransformations.hpp"
#include "cordicTable.h"

Current::Current(uint8_t alignment)
{
	this->alignment = alignment;
}
void Current::setCurrents(uint32_t vaCnt, uint32_t vbCnt)
{
	this->vaCnt = (int32_t)vaCnt;
	this->vbCnt = (int32_t)vbCnt;
}

void Current::calculateCurrents(int32_t electricalAngle)
{
	ia = calibration(vaCnt, ADC1_OFFSET_MEAN, ADC1_OFFSET_STD);
	ib = calibration(vbCnt, ADC2_OFFSET_MEAN, ADC2_OFFSET_STD);
//	ia = filter(calibration(vaCnt, ADC1_OFFSET_MEAN, ADC1_OFFSET_STD), FILTER_A, FILTER_B, filA);
//	ib = filter(calibration(vbCnt, ADC2_OFFSET_MEAN, ADC2_OFFSET_STD), FILTER_A, FILTER_B, filB);
	ic = - (ia + ib);
	transformations::abc2dq(ia, ib, ic, electricalAngle, alignment, &id, &iq);
}

int32_t Current::calibration(int32_t adcCount, int32_t offsetMean, int32_t offsetStd)
{
	int32_t upperLimit = offsetMean + 3 * offsetStd;
	int32_t lowerLimit = offsetMean - 3 * offsetStd;
	int32_t current;

	if((int32_t)(adcCount) > lowerLimit && (adcCount) < upperLimit)
	{
		current = 0;
	}

	else
	{
		// current = (adcCount - offsetMean) * SENSOR_RANGE / maxCount; PERO, en unidades de CORDIC_MUL
		current = (int32_t)(adcCount) - offsetMean;
		current *= (int32_t)(SENSOR_RANGE);
		current <<= (CORDIC_MUL_FACTOR -12);

	}

	return current;
}

int32_t Current::filter(int32_t u, int32_t a, int32_t b, int32_t* state)
{

	int32_t y=0;

	y= b * (u+state[0]) +  a * state[1];
	state[0] = u;
	y>>=CORDIC_MUL_FACTOR;
	state[1]= y;
	return y;
}

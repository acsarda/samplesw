/*
 * encoder.cpp
 *
 *  Created on: Jul 17, 2022
 *      Author: augusto
 */

#include "encoder.hpp"

uint32_t cntZero = 1 << 15;
uint32_t cntRange = 8000;
//int32_t  cntZeroOffset = 0;

Encoder::Encoder(TIM_HandleTypeDef * htim)
{
	this->timHandler = htim;
}
/**
 * @brief Reset of the encoder counter register when the signalZflag is set.
 * The status of the flag is managed by the callback function that attends
 * the interruptions caused by the the signal z in the GPIO pin.
 * The callback function is HAL_GPIO_EXTI_Callback (see main)
 */
void Encoder::zSignalEvent()
{
	timHandler->Instance->CNT = cntZero;
}

void Encoder::signalDataProcessing()
{
	int32_t cntShifted = (int32_t)timHandler->Instance->CNT - (int32_t)cntZero;

	//calculateMechanicalPosition(cntShifted);
	calculateElectricalPosition(cntShifted);
	//calculateMechanicalSpeed();
	//calculateElectricalSpeed();
}

void Encoder::calculateSpeed(void)
{
	cntPrevious = cntActual;
	cntActual = timHandler->Instance->CNT;

	if(cntActual > cntPrevious) // clockwise turning
	{
		mechanicalSpeed = cntActual - cntPrevious;
	}
	else   			   			// counterclockwise turning
	{
		mechanicalSpeed = cntPrevious - cntActual;
	}
	if(mechanicalSpeed > 4000)
	{
		mechanicalSpeed = 8000 - mechanicalSpeed;
		if(mechanicalSpeed > 8000)
		{
			return;
		}
	}

//	if(mechanicalPosition > 256)
//	   mechanicalPosition =0 ;
	/*
	 * convierto esta medicion en cuentas a [rad/s] multiplicado por CORDIC_MUL
	 * mechanicalSpeed [Hz] = mechanicalSpeed [cuentas de arriba] * 500 / 8000
	 * mechanicalSpeed [rad/s] = 2 * pi * mechanicalSpeed [Hz];
	 *
	 * Ver que 500 / 8000 = 1 / 16
	 * Sorry not sorry for the magic numbers
	*/
	mechanicalSpeed *= (2 * CORDIC_PI );
	mechanicalSpeed >>= 4; // i.e / 16
	electricalSpeed = POLES_PAIRS * mechanicalSpeed;
}

/*
 * @brief
 * Calculates the mechanical position in radians multiply by CORDIC_MUL units.
 * */
void Encoder::calculateMechanicalPosition(int32_t counter)
{
	if(counter > 0) // CCW turning
	{
		mechanicalPosition = counter - cntRange;
	}
	else            // CW turning
	{
		mechanicalPosition = counter;
	}
	mechanicalPosition *= -1;
	mechanicalPosition *= 2 * (int32_t)CORDIC_PI;
	mechanicalPosition /= (int32_t)cntRange;
	mechanicalPosition -= CORDIC_PI;

	mechanicalPosition -= mechanicalPositionOffset;

	if(mechanicalPosition > (int32_t)CORDIC_PI)
	{
		mechanicalPosition -= 2 * CORDIC_PI;
	}

	if(mechanicalPosition < -(int32_t)CORDIC_PI)
	{
		mechanicalPosition += 2 * CORDIC_PI;
	}
}


/*
 * @brief
 * Calculates the electrical position in radians multiply by CORDIC_MUL units.
 * */
void Encoder::calculateElectricalPosition(int32_t counter)
{
	electricalPosition = POLES_PAIRS * counter;
	electricalPosition %= (int32_t)cntRange;
	electricalPosition *= - 2 * (int32_t)CORDIC_PI;
	electricalPosition /= (int32_t)cntRange;

	electricalPosition -= electricPositionOffset;

	if(electricalPosition > (int32_t)CORDIC_PI)
	{
		electricalPosition -= 2 * CORDIC_PI;
	}
	if(electricalPosition < - (int32_t)CORDIC_PI)
	{
		electricalPosition += 2 * CORDIC_PI;
	}
}

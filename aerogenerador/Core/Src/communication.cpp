/*
 * communication2.cpp
 *
 *  Created on: Nov 14, 2022
 *      Author: augusto
 */

#include <communication.hpp>

Communication2::Communication2(UART_HandleTypeDef *h,
							   Encoder *encoderInstance,
							   Current *currentInstance,
							   Controller *velControllerInstance,
							   DirectController *idControllerInstance,
							   QuadratureController *iqControllerInstance,
							   int64_t *power,
							   int32_t *mechanicalSpeedRef)
{
	huart = h;

	this->encoderInstance = encoderInstance;
	this->currentInstance = currentInstance;
	this->velControllerInstance = velControllerInstance;
	this->idControllerInstance = idControllerInstance;
	this->iqControllerInstance = iqControllerInstance;
	this->power = power;
	this->mechanicalSpeedRef = mechanicalSpeedRef;

	// initial conditions
	state = StateMENU;
	flags.transmitionFree = true;
	flags.receiveFree = false;
}

void Communication2::checkState()
{
	if(flags.receiveFree)
	{
	/*
	bufferMenu[0] -> header
	bufferMenu[0] -> state
	*/
		if(bufferMenu[0] == HEADER)
		{
			switch(bufferMenu[1])
			{
			case StateMENU:
				state = StateMENU;
				break;
			case StateCONTINUOUS:
				state = StateCONTINUOUS;
				setPointAngularSpeed = (uint32_t)bufferMenu[2];
				// setPointAngularSpeed recibido es en [Hz] multiplicado por 8 (para tener mas resolucion)
				// lo convierto a rad/s multiplicado por CORDIC_MUL
				setPointAngularSpeed *= 2 * CORDIC_PI;
				setPointAngularSpeed >>= 3; // /8
				break;
			case StateBATCH:
				state = StateBATCH;
				variables = bufferMenu[2];

				flags.loadingBuffersBatch = true;
				positionBufferBatch = 2;
				break;
			}
			// clean bufferMenu
			for(uint8_t idx = 0; idx < LENGTHMENU; idx++)
			{
				bufferMenu[idx] = RESET;
			}

		}
		HAL_UART_Receive_IT(huart, bufferMenu, LENGTHMENU);
		flags.receiveFree = false;
	}
}

void Communication2::responseContinuousMechanicalSpeedRef()
{
	setContinuousData(encoderInstance->getMechanicalSpeed(),
					  (int16_t)encoderInstance->getElectricalPosition(),
					  (int16_t)(*power >> 5),
					  (int16_t)(velControllerInstance->getControlAction() >>5),
					  (int16_t)(currentInstance->getId()>>5),
					  (int16_t)(currentInstance->getIq()>>5),
					  (int16_t)(idControllerInstance->getControlAction()>>5),
					  (int16_t)(iqControllerInstance->getControlAction()>>5));
	// TODO wmref
	if(flags.transmitionFree)
	{
		flags.transmitionFree = false;
		HAL_UART_Transmit_IT(huart, bufferContinuous, LENGTHCONTINUOUS);
	}
}

void Communication2::responseContinuousMPPT()
{
	setContinuousData(encoderInstance->getMechanicalSpeed(),
					  (int16_t)(*power >> 5),
					  (int16_t)(*mechanicalSpeedRef >> 5),
					  (int16_t)(velControllerInstance->getControlAction() >>5),
					  (int16_t)(currentInstance->getId()>>5),
					  (int16_t)(currentInstance->getIq()>>5),
					  (int16_t)(idControllerInstance->getControlAction()>>5),
					  (int16_t)(iqControllerInstance->getControlAction()>>5));
	// TODO wmref
	if(flags.transmitionFree)
	{
		flags.transmitionFree = false;
		HAL_UART_Transmit_IT(huart, bufferContinuous, LENGTHCONTINUOUS);
	}
}

void Communication2::setContinuousData(const uint32_t a,
									   const int16_t b,
									   const int16_t c,
									   const int16_t d,
									   const int16_t e,
									   const int16_t f,
									   const int16_t g,
									   const int16_t h)
{
	uint8_t position = 1; // begins in 1 because position 0 is the header
	uint8_t checksum;

	bufferContinuous[0] = HEADER;

	*(uint32_t*)(bufferContinuous + position) = a;

	position += LENGTHWORD4;
	*(int16_t*)(bufferContinuous + position) = b;
	position += LENGTHWORD2;
	*(int16_t*)(bufferContinuous + position) = c;
	position += LENGTHWORD2;
	*(int16_t*)(bufferContinuous + position) = d;
	position += LENGTHWORD2;
	*(int16_t*)(bufferContinuous + position) = e;
	position += LENGTHWORD2;
	*(int16_t*)(bufferContinuous + position) = f;

	position += LENGTHWORD2;
	*(int16_t*)(bufferContinuous + position) = g;
	position += LENGTHWORD2;
	*(int16_t*)(bufferContinuous + position) = h;
	position += LENGTHWORD2;

	for(uint8_t idx = 1; idx < LENGTHCONTINUOUS - 1; idx++)
	{
		checksum += bufferContinuous[idx];
	}
	bufferContinuous[LENGTHCONTINUOUS - 1] = checksum;
}

void Communication2::loadBuffersBatch()
{
	if(flags.loadingBuffersBatch)
	{
		switch(variables)
		{
		case ia_ib_elecAngle:
			word2buffer((int16_t)currentInstance->getIa(), bufferVariable1 + positionBufferBatch);
			word2buffer((int16_t)currentInstance->getIb(), bufferVariable2 + positionBufferBatch);
			word2buffer((int16_t)encoderInstance->getElectricalPosition(), bufferVariable3 + positionBufferBatch);
			break;
		case iqRef_iq_id:
			word2buffer((int16_t)0, bufferVariable1 + positionBufferBatch); // TODO wmRef
			word2buffer((int16_t)currentInstance->getIq(), bufferVariable2 + positionBufferBatch);
			word2buffer((int16_t)currentInstance->getId(), bufferVariable3 + positionBufferBatch);
			break;
		case mechSpeedRef_mechSpeed:
			word2buffer((int16_t)0, bufferVariable1 + positionBufferBatch); // TODO wmRef
			word2buffer((int16_t)encoderInstance->getMechanicalSpeed(), bufferVariable2 + positionBufferBatch);
			break;
		}
		positionBufferBatch += LENGTHWORD2;
	}
	if(flags.loadingBuffersBatch && positionBufferBatch >= LENGTHBATCH - 1)
	{
		bufferVariable1[0] = HEADER;
		bufferVariable2[0] = HEADER;
		bufferVariable3[0] = HEADER;

		bufferVariable1[1] = variable1st;
		bufferVariable2[1] = variable2nd;
		bufferVariable3[1] = variable3rd;

		// checksums
		bufferVariable1[LENGTHBATCH - 1] = 0;
		bufferVariable2[LENGTHBATCH - 1] = 0;
		bufferVariable3[LENGTHBATCH - 1] = 0;
		for(uint8_t idx = 2; idx < LENGTHBATCH - 1; idx++)
		{
			bufferVariable1[LENGTHBATCH - 1] += bufferVariable1[idx];
			bufferVariable2[LENGTHBATCH - 1] += bufferVariable2[idx];
			bufferVariable3[LENGTHBATCH - 1] += bufferVariable3[idx];
		}

		flags.loadingBuffersBatch = false;
		bufferBatch2transmit = variable1st;
	}
}

void Communication2::responseBatch()
{
	loadBuffersBatch();
	if(!flags.loadingBuffersBatch && flags.transmitionFree)
	{
		switch(bufferBatch2transmit)
		{
		case variable1st:
			HAL_UART_Transmit_IT(huart, bufferVariable1, LENGTHBATCH);
			flags.transmitionFree = false;
			break;
		case variable2nd:
			HAL_UART_Transmit_IT(huart, bufferVariable2, LENGTHBATCH);
			flags.transmitionFree = false;
			break;
		case variable3rd:
			HAL_UART_Transmit_IT(huart, bufferVariable3, LENGTHBATCH);
			flags.transmitionFree = false;
			flags.loadingBuffersBatch = true;
			break;
		}
	}
}

void Communication2::response(bool timeFlag)
{
	if(timeFlag)
	{
		if(state == StateCONTINUOUS)
		{
			responseContinuousMechanicalSpeedRef();
//			responseContinuousMPPT();
		}
		else if(state == StateBATCH)
		{
			responseBatch();
		}
	}
}

void Communication2::word2buffer(int16_t word, uint8_t *buffer)
{
	int16_t mask = 0xFF;

	*buffer = word & mask;
	*(buffer + 1) = (word >> 8) & mask;
}

void Communication2::word2buffer(uint16_t word, uint8_t *buffer)
{
	uint16_t mask = 0xFF;

	*buffer = word & mask;
	*(buffer + 1) = (word >> 8) & mask;
}

void Communication2::word2buffer(uint32_t word, uint8_t *buffer)
{
	uint32_t mask = 0xFFFF;

	*buffer =               word & mask;
	*(buffer + 1) = (word >> 8)  & mask;
	*(buffer + 2) = (word >> 16) & mask;
	*(buffer + 3) = (word >> 24) & mask;
}

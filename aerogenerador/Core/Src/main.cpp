/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include <communication.hpp>
#include <main.h>
#include "tim.h"
#include "usart.h"
#include "gpio.h"
#include "adc.h"
#include "dma.h"
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

#include "actuators.hpp"
#include "parameters.h"
//#include "communication.hpp"
#include "encoder.hpp"
#include "currents.hpp"
#include "controller.hpp"
#include "svPWM.hpp"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
typedef enum
{
	HERTZ1 = 4000u,
	HERTZ2 = 2000u,
	HERTZ10 = 400u,
	HERTZ100 = 40u,
	HERTZ250 = 16u,
	HERTZ500 = 8u
} FREQUENCY;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define ALPHA_POWER ((1 * CORDIC_MUL) / 1000)     // alpha para el filtro exponencial
#define ALPHA_MECH_SPEED ((1 * CORDIC_MUL) / 100) // alpha para el filtro exponencial
#define MECH_SPEED_MIN 536167    // 625 RPM
#define MECH_SPEED_MAX 686293    // 800 RPM
#define MECH_SPEED_DELTA (858*5) // 1 RPM * 5
#define POWER_THRESHOLD 1815     // stds, siendo std = 0.2216 W

#define PSI_PM 30409   // = 0.029 Wb ~ 2^20
// #define FACTOR 1048576 // = 2^20
#define FACTOR 20

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
int32_t abs(int32_t x);
int32_t sign(int32_t x);
int64_t exponentialSmoothing(int64_t, int64_t, int64_t);
int32_t calculatePower(int32_t iq, int32_t elecSpeed);
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
struct
{
	bool twentyMicroSecs;
	bool hertz2;
	bool hertz1;
	bool hertz10;
	bool hertz100;
	bool hertz250;
	bool hertz500;
	bool hertz4k;
} flagsTIM2 = {false, false, false, false, false, false, false};

struct
{
	uint32_t encoderSpeed;
	uint32_t hertz1;
	uint32_t hertz2;
	uint32_t hertz10;
	uint32_t hertz100;
	uint32_t hertz250;
	uint32_t hertz500;
} cntPreviousTIM2 = {RESET, RESET, RESET, RESET, RESET, RESET, RESET};

bool ADCFlag = RESET, ADCBusy=RESET,StopPWMOnce = SET, StartPWMOnce= SET;
uint32_t cntTIM2 = 0;

// sensors
uint32_t cntEncoderPrev = 0, cntEncoder = 0;
uint32_t speed;
Encoder encoder(&htim4);
Current currents(a2d);
//DirectController     idController(13422, 3 * CORDIC_MUL, 3 * CORDIC_MUL); // Ts = 0.2ms * CORDIC_MUL^2 = 13421.7
//QuadratureController iqController(13422, 3 * CORDIC_MUL, 3 * CORDIC_MUL); // Ts = 0.2ms * CORDIC_MUL^2 = 13421.7
uint32_t busDCVoltage =24*CORDIC_MUL; //24 * CORDIC_F;
DirectController     idController(CORDIC_MUL*2, 30 , -23*CORDIC_MUL, 23*CORDIC_MUL);
QuadratureController iqController(CORDIC_MUL*2, 30 , 0, 23*CORDIC_MUL);

Controller VelController(CORDIC_MUL*0.2,30, -20*CORDIC_MUL, 0); // como generador
//Controller VelController(CORDIC_MUL*0.2,30, 0, 20*CORDIC_MUL);    // como motor

struct filterVariables{
	int64_t calculation;
	int64_t filter;
	int64_t filter1;
	uint8_t firstFilter = SET;
} power, mechSpeed;

int32_t mechanicalSpeedRef = 0;

// actuator
uint32_t Ts = 1 << 10;

SpaceVectorPWM svm(&htim1,
		           TIM_CHANNEL_1,
				   TIM_CHANNEL_2,
				   TIM_CHANNEL_3,
				   busDCVoltage,
				   a2d,
				   Ts);

int32_t elecPos = 0;
volatile int32_t elecPosDebug = 0;

//TransmitContinuous uartContinuous(&huart3);
//TransmitBuffer uartBuffer(&huart3);
Communication2 uart(&huart3, &encoder, &currents, &VelController, &idController, &iqController, &power.filter, &mechanicalSpeedRef);
uint32_t pDataADC;

// algoritmo referencia
int32_t power1 = RESET, powerDelta = RESET;
int32_t angSpeedRef = RESET, angSpeedRef1 = RESET, angSpeedDelta = RESET, angSpeedDelta1 = -MECH_SPEED_DELTA;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
void timeControl();

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */


  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_TIM4_Init();
  MX_USART3_UART_Init();
  MX_ADC1_Init();
  MX_ADC2_Init();
  MX_DMA_Init();
  /* USER CODE BEGIN 2 */
//  HAL_TIM_Base_Start_IT(&htim1);
  HAL_TIM_Base_Start_IT(&htim2);
  HAL_UART_Init(&huart3);

  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_2);
  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_3);

  HAL_TIMEx_PWMN_Start(&htim1, TIM_CHANNEL_1);
  HAL_TIMEx_PWMN_Start(&htim1, TIM_CHANNEL_2);
  HAL_TIMEx_PWMN_Start(&htim1, TIM_CHANNEL_3);

  HAL_TIM_Encoder_Start(&htim4, TIM_CHANNEL_ALL);

  HAL_ADC_MspDeInit(&hadc1);
  HAL_ADC_MspInit(&hadc1);
  HAL_ADCEx_Calibration_Start(&hadc1);

  HAL_ADC_MspDeInit(&hadc2);
  HAL_ADC_MspInit(&hadc2);
  HAL_ADCEx_Calibration_Start(&hadc2);

  // La tension que se quiera aplicaren [V] en vd, vq, hay que multiplicarla por CORDIC_MUL
  int32_t vd = 0* CORDIC_MUL; //
  int32_t vq = 0* CORDIC_MUL;
  int32_t iq= 0,id=0,elecvel=0;
  idController.setReference(0 * CORDIC_MUL);
  iqController.setReference(0 * CORDIC_MUL);
  VelController.setReference(0 * CORDIC_MUL);
  HAL_UART_Receive_IT(&huart3, uart.bufferMenu, LENGTHMENU);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while(true)
  {
	  timeControl();
	  if(ADCFlag)
	  {
		  ADCFlag = RESET;

		  elecPos = encoder.getElectricalPosition();
		  elecvel = encoder.getElectricalSpeed();
		  currents.calculateCurrents(elecPos);
		  iq = -currents.getIq();
		  id = -currents.getId();

		  if(uart.state)
		  {
			  StopPWMOnce = SET;
			  if(StartPWMOnce)
			  {
				  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
				  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_2);
				  HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_3);
				  HAL_TIMEx_PWMN_Start(&htim1, TIM_CHANNEL_1);
				  HAL_TIMEx_PWMN_Start(&htim1, TIM_CHANNEL_2);
				  HAL_TIMEx_PWMN_Start(&htim1, TIM_CHANNEL_3);
				  StartPWMOnce = RESET;
			  }
			  idController.calculateDecoupling(elecvel, iq);
			  iqController.calculateDecoupling(elecvel, id);
			  vd = idController.controlPI(id);
			  vq = iqController.controlPI(iq);

			  power.calculation = (int32_t)(((int64_t)(iq * vq)) >> CORDIC_MUL_FACTOR) + (((int64_t)(id * vd)) >> CORDIC_MUL_FACTOR);
			  power.calculation *= 3;
			  power.calculation /= 2;

//			  power.calculation = (((int64_t)(-iq)) * ((int64_t)vq)) >> CORDIC_MUL_FACTOR;

//			  condicion inicial para usar el filto exponencial en la potencia
			  if(power.firstFilter)
			  {
				  power.firstFilter = RESET;
				  power.filter1 = power.calculation;
			  }
			  else
			  {
				  power.filter = exponentialSmoothing(power.calculation, power.filter1, ALPHA_POWER);
				  power.filter1 = power.filter;
			  }
		  }
		  else
		  {
			  StartPWMOnce= SET;

//			  reinicio los filtros
			  power.firstFilter = SET;
			  mechSpeed.firstFilter = SET;

			  if(StopPWMOnce)
			  {
				  HAL_TIM_PWM_Stop(&htim1, TIM_CHANNEL_1);
				  HAL_TIM_PWM_Stop(&htim1, TIM_CHANNEL_2);
				  HAL_TIM_PWM_Stop(&htim1, TIM_CHANNEL_3);
				  HAL_TIMEx_PWMN_Stop(&htim1, TIM_CHANNEL_1);
				  HAL_TIMEx_PWMN_Stop(&htim1, TIM_CHANNEL_2);
				  HAL_TIMEx_PWMN_Stop(&htim1, TIM_CHANNEL_3);
				  StopPWMOnce = RESET;
			  }
			  vd=0;
			  vq=0;
			  idController.reset();
			  iqController.reset();
		  }

		  if(elecPos >= -CORDIC_PI && elecPos <= CORDIC_PI)
		  {
//			  manera facil para calibrar las cuentas del encoder (offset)
//			  svm.update(0*CORDIC_MUL, 5*CORDIC_MUL, elecPosDebug);


//			  como motor / generador
			  svm.update(vq, vd, elecPos);
		  }
		  svm.updatePWMOutput();
		  HAL_GPIO_WritePin(testBaseTime_GPIO_Port, testBaseTime_Pin,  GPIO_PIN_RESET);
	  }
	  uart.checkState();

	  if(flagsTIM2.hertz4k)
	  {
		  flagsTIM2.hertz4k = RESET;
		  if(ADCBusy) continue;

		  HAL_GPIO_WritePin(testBaseTime_GPIO_Port, testBaseTime_Pin, GPIO_PIN_SET);

		  ADCBusy = SET;
		  HAL_ADC_Start(&hadc2);
		  HAL_ADCEx_MultiModeStart_DMA(&hadc1, &pDataADC, 1);

		  encoder.signalDataProcessing();
	  }
	  if(flagsTIM2.hertz500)
	  {
		  encoder.calculateSpeed();
		  mechSpeed.calculation = encoder.getMechanicalSpeed();

		  if(mechSpeed.firstFilter)
		  {
			  mechSpeed.firstFilter = RESET;
			  mechSpeed.filter1 = mechSpeed.calculation;
		  }
		  else
		  {
			  mechSpeed.filter = exponentialSmoothing(mechSpeed.calculation, mechSpeed.filter1, ALPHA_MECH_SPEED);
			  mechSpeed.filter1 = mechSpeed.filter;
		  }

		  uart.response(flagsTIM2.hertz500);
		  flagsTIM2.hertz500= RESET;
	}

	if(flagsTIM2.hertz2)
	{
		HAL_GPIO_TogglePin(LEDGreenBoard_GPIO_Port, LEDGreenBoard_Pin);
	}

//	algoritmo para recibir comandos de referencia de velocidad mecanica
//	recordar cambiar Communication2::response
	if(flagsTIM2.hertz100)
	{
		if(uart.state)
		{
			VelController.setReference((int32_t)uart.setPointAngularSpeed); // comentar si se usa el algoritmo MPPT
			iqController.setReference(VelController.controlPI(mechSpeed.filter));
		}
		else
		{
			iqController.setReference(0);
			VelController.reset();
		}
	}

//  algoritmo MPPT
//	recordar cambiar Communication2::response
	if(flagsTIM2.hertz10)
	{
//		if(uart.state)
//		{
//			powerDelta = abs(power.filter) - abs(power1);
//
//			if(abs(powerDelta) > POWER_THRESHOLD)
//			{
////				angSpeedDelta = sign(angSpeedDelta1) * sign((int32_t)powerDelta) * MECH_SPEED_DELTA;
//				if((angSpeedDelta1 < 0 && powerDelta > 0) || (angSpeedDelta1 > 0 && powerDelta < 0))
//				{
//					angSpeedDelta = -MECH_SPEED_DELTA;
//				}
//				else if((angSpeedDelta1 < 0 && powerDelta < 0) || (angSpeedDelta1 > 0 && powerDelta > 0))
//				{
//					angSpeedDelta = MECH_SPEED_DELTA;
//				}
//				mechanicalSpeedRef += angSpeedDelta;
//
//				if(mechanicalSpeedRef < MECH_SPEED_MIN)
//				{
//					mechanicalSpeedRef = MECH_SPEED_MIN;
//					angSpeedDelta *= -1;
//				}
////				if(mechanicalSpeedRef > MECH_SPEED_MAX)
////				{
////					mechanicalSpeedRef = MECH_SPEED_MAX;
////				}
//
//				VelController.setReference(mechanicalSpeedRef);
//				iqController.setReference(VelController.controlPI(mechSpeed.filter));
//			}
//			angSpeedDelta1 = angSpeedDelta;
//			power1 = power.filter;
//		}
//		else
//		{
////			condicion inicial para usar en el algoritmo MPPT (comentar si no usa MPPT)
//			power1 = power.filter1;
//			mechanicalSpeedRef = mechSpeed.filter1;
//			VelController.setReference(mechanicalSpeedRef);
//			iqController.setReference(VelController.controlPI(mechSpeed.filter));
//		}
	}

	/* USER CODE END WHILE */

/* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}
/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV6;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
	  Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	/**
	 * the TIM2 base time interruption is every 0.2 ms = 20 us (5kHz)
	 */
	if(htim == &htim2)
	{

		flagsTIM2.hertz4k = SET;

		cntTIM2++;
	}
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	//htim4.Instance->CNT = 0;
	encoder.zSignalEvent();
}

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
	/*
	 * @brief
	 * El ADC esta configurado en modo simultaneo. El Data Register (uint32_t) del ADC1 almacena las
	 * conversiones de cada ADC (el 1 y el 2, cada uno de uint16_t)
	 * Por lo tanto, usando mascaras obtengo el valor de cada ADC.
	 */
	if(hadc == &hadc1)
	{
		uint32_t adc1, adc2;
		uint32_t mask = 0xFFFF;

		HAL_ADCEx_MultiModeStop_DMA(&hadc1);
		HAL_ADC_Stop_IT(&hadc2);

		adc1 = pDataADC & mask;
		adc2 = (pDataADC >> 16) & mask;
		ADCFlag = SET;
		ADCBusy = RESET;
		currents.setCurrents(adc1, adc2);
	}
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
	if(huart == uart.huart)
	{
		uart.flags.transmitionFree = true;

		if(uart.getState() == StateBATCH)
		{
			uart.bufferBatch2transmit++;
		}
	}
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if(huart == uart.huart)
	{
		uart.flags.receiveFree = true;
	}
}

//variable = (condition) ? expressionTrue : expressionFalse;
int32_t abs(int32_t x)
{
	return (x < 0) ? -x : x;
}

int32_t sign(int32_t x)
{
	return (x < 0) ? -1 : 1;
}

/* @brief Filtro exponencial
 * s[k] = alpha * x[k] * (1 - alpha) * s[k-1], donde
 * x[k] es la senal actual
 * s[k-1] es la respuesta anterior del filtro *
 * Referencia https://en.wikipedia.org/wiki/Exponential_smoothing
 * */
int64_t exponentialSmoothing(int64_t x, int64_t s1, int64_t alpha)
{
	return s1 + ((alpha * (x - s1)) >> CORDIC_MUL_FACTOR);
}

void timeControl()
{
	int32_t diff;

	diff = cntTIM2 - cntPreviousTIM2.hertz2;
	if(abs(diff) > HERTZ2)
	{
		flagsTIM2.hertz2 = true;
		cntPreviousTIM2.hertz2 = cntTIM2;
	}
	else
	{
		flagsTIM2.hertz2 = false;
	}

	diff = cntTIM2 - cntPreviousTIM2.hertz1;
	if(abs(diff) > HERTZ1)
	{
		flagsTIM2.hertz1 = true;
		cntPreviousTIM2.hertz1 = cntTIM2;
	}
	else
	{
		flagsTIM2.hertz1 = false;
	}

	diff = cntTIM2 - cntPreviousTIM2.hertz10;
	if(abs(diff) > HERTZ10)
	{
		flagsTIM2.hertz10 = true;
		cntPreviousTIM2.hertz10 = cntTIM2;
	}
	else
	{
		flagsTIM2.hertz10 = false;
	}

	diff = cntTIM2 - cntPreviousTIM2.hertz100;
	if(abs(diff) > HERTZ100)
	{
		flagsTIM2.hertz100 = true;
		cntPreviousTIM2.hertz100 = cntTIM2;
	}
	else
	{
		flagsTIM2.hertz100 = false;
	}

	diff = cntTIM2 - cntPreviousTIM2.hertz250;
	if(abs(diff) > HERTZ250)
	{
		flagsTIM2.hertz250 = true;
		cntPreviousTIM2.hertz250 = cntTIM2;
	}

	diff = cntTIM2 - cntPreviousTIM2.hertz500;
	if(abs(diff) > HERTZ500)
	{
		flagsTIM2.hertz500 = true;
		cntPreviousTIM2.hertz500 = cntTIM2;
	}

}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
	/* USER CODE BEGIN Error_Handler_Debug */
	  /* User can add his own implementation to report the HAL error return state */
	  __disable_irq();
	  while (1)
	  {
	  }
	  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

#include "svPWM.hpp"
#include "electricalTransformations.hpp"

extern "C"
{
#include "cordicTrigonometric.h"
}

SpaceVectorPWM::SpaceVectorPWM(TIM_HandleTypeDef * htim, uint8_t channelA, uint8_t channelB, uint8_t channelC, uint32_t busDCValue, uint8_t alignment, uint32_t Ts)
{
	int32_t sin60, cos;

	this->htim = htim;
	channels[0] = channelA;
	channels[1] = channelB;
	channels[2] = channelC;
    this->Vdc = busDCValue;
    this->alignment = alignment;
    this->Ts = Ts;
    this->Ts2 = this->Ts >> 1;

    this->VxMod = (Vdc << 1) / 3; // = (2/3) * Vdc

    CORDICsincos(CORDIC_PI_1_3, CORDIC_1F, &sin60, &cos);
    this->vRefMax = (VxMod * sin60) >> CORDIC_MUL_FACTOR;
}

void SpaceVectorPWM::update(int32_t vq, int32_t vd, int32_t electricalAngle)
{
    this->vq = vq;
    this->vd = vd;

    //  el orden en que las siguientes funciones son llamadas es importante
    this->park2clarke(electricalAngle);
    cartesian2polar();
    angle2sector();
    alphaAngle();
    timesCalculation();
    modulation();
}

// TODO esto tiene que ser una funcion independiente de esta clase
void SpaceVectorPWM::park2clarke(int32_t electricalAngle)
{
//    int32_t sinAng, cosAng;
//    CORDICsincos(electricAngle, CORDIC_1F, &sinAng, &cosAng);
//
//    if(alignment == a2q)
//    {
//        this->vAlpha =  ((sinAng * this->vd) >> CORDIC_MUL_FACTOR) + ((cosAng * this->vq) >> CORDIC_MUL_FACTOR);
//        this->vBeta  = -((cosAng * this->vd) >> CORDIC_MUL_FACTOR) + ((sinAng * this->vq) >> CORDIC_MUL_FACTOR);
//    }
//    else if(alignment == a2d)
//    {
//        this->vAlpha = ((cosAng * this->vd) >> CORDIC_MUL_FACTOR) - ((sinAng * this->vq) >> CORDIC_MUL_FACTOR);
//        this->vBeta  = ((sinAng * this->vd) >> CORDIC_MUL_FACTOR) + ((cosAng * this->vq) >> CORDIC_MUL_FACTOR);
//    }
	transformations::park2clarke(vd, vq, electricalAngle, alignment, &vAlpha, &vBeta);
}

void SpaceVectorPWM::cartesian2polar()
{
    CORDICatan2sqrt(&(this->angleAlphaBeta), &(this->vRef), this->vBeta, this->vAlpha);
}

void SpaceVectorPWM::angle2sector()
{
    if(this->angleAlphaBeta >= 0 && this->angleAlphaBeta < CORDIC_PI_1_3)
    {
        this->sector = 1;
    }
    else if(this->angleAlphaBeta >= CORDIC_PI_1_3 && this->angleAlphaBeta < CORDIC_PI_2_3)
    {
        this->sector = 2;
    }
    else if(this->angleAlphaBeta >= CORDIC_PI_2_3 && this->angleAlphaBeta < CORDIC_PI)
    {
        this->sector = 3;
    }
    else if(this->angleAlphaBeta >= - CORDIC_PI && this->angleAlphaBeta < - CORDIC_PI_2_3)
    {
        this->sector = 4;
    }
    else if(this->angleAlphaBeta >= - CORDIC_PI_2_3 && this->angleAlphaBeta < - CORDIC_PI_1_3)
    {
        this->sector = 5;
    }
    else if(this->angleAlphaBeta >= - CORDIC_PI_1_3 && this->angleAlphaBeta < 0)
    {
        this->sector = 6;
    }
}
void SpaceVectorPWM::alphaAngle()
{
    int8_t sectorAux;

    if(this->angleAlphaBeta >= 0)
    {
        this->alpha = this->angleAlphaBeta - (this->sector - 1) * CORDIC_PI_1_3;
    }
    else
    {
        switch (this->sector)
        {
            case 4:
                sectorAux = -3;
                break;
            case 5:
                sectorAux = -2;
                break;
            case 6:
                sectorAux = -1;
                break;
        }
        this->alpha = this->angleAlphaBeta - sectorAux * CORDIC_PI_1_3;
    }
}

void SpaceVectorPWM::timesCalculation()
{
    int32_t sin, cos;
	int32_t sin60;
	uint32_t t;
	int32_t factor;

	if(vRef > vRefMax)
	{
		vRef = vRefMax;
	}

	CORDICsincos(CORDIC_PI_1_3, CORDIC_1F, &sin60, &cos);

	factor = vRef * Ts;
	factor /= VxMod;

	factor *= CORDIC_MUL;
	factor /= sin60;

	CORDICsincos(CORDIC_PI_1_3 - alpha, CORDIC_1F, &sin, &cos);
	t = sin * factor;
	t /= CORDIC_MUL;
	t1 = (t > Ts)? 0 : t;

	CORDICsincos(alpha, CORDIC_1F, &sin, &cos);
	t = sin * factor;
	t /= CORDIC_MUL;
	t2 = (t > Ts)? 0 : t;

	t0 = (Ts < t1 + t2)? 0 : Ts - (t1 + t2);
}

void SpaceVectorPWM::modulation()
{
	int32_t t02 = t0 >> 1; // = t0 / 2
	switch(sector)
	{
		case 1:
			pulse[0] = t1 + t2 + t02;
			pulse[1] = t2 + t02;
			pulse[2] = t02;
			break;
		case 2:
			pulse[0] = t1 + t02;
			pulse[1] = t1 + t2 + t02;
			pulse[2] = t02;
			break;
		case 3:
			pulse[0] = t02;
			pulse[1] = t1 + t2 + t02;
			pulse[2] = t2 + t02;
			break;
		case 4:
			pulse[0] = t02;
			pulse[1] = t1 + t02;
			pulse[2] = t1 + t2 + t02;
			break;
		case 5:
			pulse[0] = t2 + t02;
			pulse[1] = t02;
			pulse[2] = t1 + t2 + t02;
			break;
		case 6:
			pulse[0] = t1 + t2 + t02;
			pulse[1] = t02;
			pulse[2] = t1 + t02;
			break;
		default:
			pulse[0] = 0;
			pulse[1] = 0;
			pulse[2] = 0;
			break;
	}
//	pulse[0] >>= 1;
//	pulse[1] >>= 1;
//	pulse[2] >>= 1;
}

void SpaceVectorPWM::startPWMOutput()
{
	for(uint8_t idx = 0; idx < 3; idx++)
	{
		HAL_TIM_PWM_Start(htim, channels[idx]);
	}
}

void SpaceVectorPWM::setPulse(uint8_t channel, uint32_t pulse)
{
	switch(channel)
	{
	case TIM_CHANNEL_1:
		htim->Instance->CCR1 = pulse;
		break;
	case TIM_CHANNEL_2:
		htim->Instance->CCR2 = pulse;
		break;
	case TIM_CHANNEL_3:
		htim->Instance->CCR3 = pulse;
		break;
	case TIM_CHANNEL_4:
		htim->Instance->CCR4 = pulse;
		break;
	}
}

void SpaceVectorPWM::updatePWMOutput()
{
	for(uint8_t idx = 0; idx < 3; idx++)
	{
		setPulse(channels[idx], pulse[idx]);
	}
}

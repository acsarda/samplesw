/*
 * actuators.cpp
 *
 *  Created on: Jun 27, 2022
 *      Author: augusto
 */
#include <actuators.hpp>
#include "svPWM.hpp"

//void actuatorsUpdate(void)
//{
//	updatePWM();
//}
void actuator::PWM::init(TIM_HandleTypeDef * handlerTIM, uint8_t channel)
{
	this->handlerTIM = handlerTIM;
	this->channel = channel;
	this->pulse = 0;
}

void actuator::PWM::setPulse(uint32_t pulseDC)
{
	this->pulse = pulseDC;
}

void actuator::PWM::updatePWM()
{
	switch(this->channel)
	{
		case TIM_CHANNEL_1:
			this->handlerTIM->Instance->CCR1 = this->pulse;
			break;
		case TIM_CHANNEL_2:
			this->handlerTIM->Instance->CCR2 = this->pulse;
			break;
		case TIM_CHANNEL_3:
			this->handlerTIM->Instance->CCR3 = this->pulse;
			break;
		case TIM_CHANNEL_4:
			this->handlerTIM->Instance->CCR4 = this->pulse;
			break;
	}
}
